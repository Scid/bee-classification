from settings import *

def Training_features():
	training_data = []
	for subfolder in Bee:
		dir = os.path.join(TRAIN_DIR, subfolder)
		if subfolder.strip('/') == 'single_bee':
			label = [1, 0]
		else:
			label = [0, 1]

		for img in tqdm(os.listdir(dir)):
			path = os.path.join(dir, img)
			img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
			training_data.append([np.array(img), np.array(label)])

	shuffle(training_data)
	np.save('train_data.npy', training_data)
	return training_data


def Testing_features():
	testing_data = []
	for subfolder in Bee:
		dir = os.path.join(TEST_DIR, subfolder)
		if subfolder.strip('/') == 'single_bee':
			label = [1, 0]
		else:
			label = [0, 1]

		for img in tqdm(os.listdir(dir)):
			path = os.path.join(dir, img)
			img_num = [img.split('.')[0]]
			img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
			img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
			testing_data.append([np.array(img), np.array(label)])

	shuffle(testing_data)
	np.save('test_data.npy', testing_data)
	return testing_data


def create_predictions(model, test_data):
	true = 0
	x_to_predict = np.array([i[0] for i in test_data]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
	true_lbl = [i[1] for i in test_data]
	prediction = model.predict(x_to_predict)
	# with open('predictions.txt', 'w') as txtfile:
	# 	for ind, item in enumerate(prediction):
	# 		txtfile.write("%s, %s\n" % (item, true_lbl[ind]))
	for ind, item in enumerate(prediction):
		if np.argmax(item) == np.argmax(true_lbl[ind]):
			true += 1
		else:
			continue
	accuracy = (true/len(prediction)) * 100
	print("Accuracy of Classification = {}".format(accuracy))
	return True
