import cv2
import numpy as np
import os
from random import shuffle
from tqdm import tqdm
import tensorflow as tf

TRAIN_DIR = "Train/"
TEST_DIR = "Test/"
Bee = ['no_bee/', 'single_bee/']
IMG_SIZE = 32
LR = 1e-3
MODEL_NAME = "Bee_Classification_model_{}_{}_Layers".format("CNN","7")
