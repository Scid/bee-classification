# Bee-Classifier - Deep Neural Networks

Bee-Pi is an initiative by the USU Computer Science professor Dr. Vladimir Kulyukin, 
to incorporate Rasberry Pi with the bee-hive system. The Pi could extract essential 
data that can be leveraged to create temprature and time-controlled algorithms and make 
the bee population of the hive thriving and healthy throughout the season. 

This is a classification project that incorporates deep neural nets (convolution NN) to
classify a set of hive images into classes of presence or absence of bees. Coded using 
TF-learn, the accuracy of the model is around 98%. 

## Tools and Packages

* [Anaconda Python Installer](https://github.com/conda/conda)
* [Tensorflow on Python](https://github.com/tensorflow/tensorflow)
* [TF-Learn wrapper for Tensorflow](https://github.com/tflearn/tflearn)
* [PyCharm IDE by JetBrians](https://www.jetbrains.com/pycharm/)
* [Open-CV for Anaconda on Windows](https://github.com/opencv/opencv)
* [Numpy - Python](https://github.com/numpy/numpy)
* [tqdm](https://pypi.python.org/pypi/tqdm)

## Supporting functions - 

An additional script that contains the function to create feature sets for 
training and testing of the model and is imported in the main script. 

Steps - 

Import supporting functions, constants and other imports
```
from settings import *
from supporting_functions import Training_features, Testing_features, create_predictions
```

## Running the main script - (First Time)
The code outouts are given in form of console images - 

### 1. Loading test and train set and extracting image features and labels

![No Bee - Train](https://github.com/Sidhant-Chatterjee/Bee-Classification/blob/master/images/snip_5_1.JPG)

![With Bee - Train](https://github.com/Sidhant-Chatterjee/Bee-Classification/blob/master/images/snip_5_2.JPG)

![No Bee - Test](https://github.com/Sidhant-Chatterjee/Bee-Classification/blob/master/images/snip_5_3.JPG)

![With Bee - Test](https://github.com/Sidhant-Chatterjee/Bee-Classification/blob/master/images/snip_5_4.JPG)

### 2. Training of Model 

![Training stats - batch change](https://github.com/Sidhant-Chatterjee/Bee-Classification/blob/master/images/snip_2.JPG)

### 3. Predicting - (First Time)
![End of training and Predicting](https://github.com/Sidhant-Chatterjee/Bee-Classification/blob/master/images/snip_3.JPG)

## Running the main script - (With saved training and test data and trained model)

### 1. When features are already extracted and a **npy** file exists for test and 
train features

![Pre-Extracted data exists](https://github.com/Sidhant-Chatterjee/Bee-Classification/blob/master/images/snip_1.JPG)

### 2. When a trained model is loaded, and used for predicting

![Predicting with model loaded](https://github.com/Sidhant-Chatterjee/Bee-Classification/blob/master/images/snip_4.JPG)

## Authors

* **Sidhant Chatterjee** - [Homepage](http://sidhant-chatterjee.net)

## Acknowledgments

* [Utah State Computer Science Department](http://www.cs.usu.edu/)
* [Professor Vladimir Kulyukin](http://digital.cs.usu.edu/~vkulyukin/)
* [Sentdx Python Tutorials](https://pythonprogramming.net/)

