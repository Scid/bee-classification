from settings import *
from supporting_functions import Training_features, Testing_features, create_predictions

def main():
	if os.path.exists('C:/Users/sidha/OneDrive/DeepLearn/Bee_Classifier/train_data.npy'):
		train_data = np.load('train_data.npy')
		print("Training Data Loaded\n")
		if os.path.exists('C:/Users/sidha/OneDrive/DeepLearn/Bee_Classifier/test_data.npy'):
			test_data = np.load('test_data.npy')
			print("Testing Data Loaded\n")
		else:
			test_data = Testing_features()
	else:
		train_data = Training_features()
		test_data = Testing_features()

	import tflearn
	from tflearn.layers.conv import conv_2d, max_pool_2d
	from tflearn.layers.core import input_data, dropout, fully_connected
	from tflearn.layers.estimator import regression

	tf.reset_default_graph()
	convnet = input_data(shape=[None, IMG_SIZE, IMG_SIZE, 1], name='input')

	convnet = conv_2d(convnet, 32, 5, activation='relu')
	convnet = max_pool_2d(convnet, 5)

	convnet = conv_2d(convnet, 64, 5, activation='relu')
	convnet = max_pool_2d(convnet, 5)

	convnet = conv_2d(convnet, 128, 5, activation='relu')
	convnet = max_pool_2d(convnet, 5)

	convnet = conv_2d(convnet, 64, 5, activation='relu')
	convnet = max_pool_2d(convnet, 5)

	convnet = conv_2d(convnet, 32, 5, activation='relu')
	convnet = max_pool_2d(convnet, 5)

	convnet = fully_connected(convnet, 1024, activation='relu')
	convnet = dropout(convnet, 0.8)

	convnet = fully_connected(convnet, 2, activation='softmax')
	convnet = regression(convnet, optimizer='adam', learning_rate=LR, loss='categorical_crossentropy', name='targets')

	model = tflearn.DNN(convnet, tensorboard_dir='log')

	if os.path.exists('C:/Users/sidha/OneDrive/DeepLearn/Bee_Classifier/{}.meta'.format(MODEL_NAME)):
		model.load(MODEL_NAME)
		print('Model - {} loaded successfully !\n'.format(MODEL_NAME))
	else:
		train = train_data[:-500]
		test = train_data[-500:]

		X = np.array([i[0] for i in train]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
		Y = [i[1] for i in train]

		test_x = np.array([i[0] for i in test]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
		test_y = [i[1] for i in test]

		model.fit({'input': X}, {'targets': Y}, n_epoch=3, validation_set=({'input': test_x}, {'targets': test_y}),
		          snapshot_step=500, show_metric=True, run_id=MODEL_NAME)

		model.save(MODEL_NAME)
	create_predictions(model, test_data)

if __name__ == "__main__":
	main()
